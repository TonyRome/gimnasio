// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCKNFskKrX4t_io4OK1PnXv1InX6TDiY0g",
    authDomain: "gimnasio-72e4a.firebaseapp.com",
    databaseURL: "https://gimnasio-72e4a.firebaseio.com",
    projectId: "gimnasio-72e4a",
    storageBucket: "gimnasio-72e4a.appspot.com",
    messagingSenderId: "198499659199",
    appId: "1:198499659199:web:0eee2d6adfbdbe98ea850a",
    measurementId: "G-CTD69RXWME"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
