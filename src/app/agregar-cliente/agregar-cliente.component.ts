import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { MensajesService } from '../service/mensajes.service';

@Component({
  selector: 'app-agregar-cliente',
  templateUrl: './agregar-cliente.component.html',
  styleUrls: ['./agregar-cliente.component.scss']
})
export class AgregarClienteComponent implements OnInit {
  porcentajeSubida : number = 0
  formularioCliente: FormGroup
  urlImg : string = ''
  esEditable: boolean = false
  id: string

  constructor(
    private fb: FormBuilder, 
    private storage: AngularFireStorage,
    private db: AngularFirestore,
    private activeRoute: ActivatedRoute,
    private msj: MensajesService
    ) { }

  ngOnInit() {



    this.formularioCliente = this.fb.group({ //Atributos del formulario deben ser igual a los atributos de la DDBB FireBase
      nombre:['',Validators.required],
      apellido:['',Validators.required],
      correo:['',Validators.compose([
        Validators.required, Validators.email
      ])],
      cedula:[''],
      fechaNacimiento:['', Validators.required],
      telefono:[''],
      img:['', Validators.required]

    })

      this.id = this.activeRoute.snapshot.params.clienteID

      if(this.id != undefined){
        
        this.esEditable = true
        
        this.db.doc<any>('clientes/'+this.id).valueChanges().subscribe((cliente)=>{
          
          this.formularioCliente.setValue({
            nombre: cliente.nombre,
            apellido: cliente.apellido,
            correo: cliente.correo,
            cedula: cliente.cedula,
            fechaNacimiento: new Date(cliente.fechaNacimiento.seconds * 1000).toISOString().substr(0,10), //Se debe rescatar de esta forma yaque el formato fecha en firebase esta en formato timestamp
            telefono: cliente.telefono,
            img: ' '        
          })

          this.urlImg = cliente.img

        })



      }

     
  }

  agregar(){



    this.formularioCliente.value.img = this.urlImg //Agrega al formulario la URL de la imagen
    this.formularioCliente.value.fechaNacimiento = new Date(this.formularioCliente.value.fechaNacimiento)

    console.log(this.formularioCliente.value)
    this.db.collection('clientes').add(this.formularioCliente.value).then((termino)=>{
      this.msj.mensajeCorrecto('Agregar','Se agrego correctamente')

    })
  }

  editar(){
    this.formularioCliente.value.img = this.urlImg //Por si no cambia la imagen
    this.formularioCliente.value.fechaNacimiento = new Date(this.formularioCliente.value.fechaNacimiento)

    this.db.doc('clientes/'+this.id).update(this.formularioCliente.value).then(()=>{
      this.msj.mensajeCorrecto('Editado','Se edito correctamente')


    }).catch(()=>{
      this.msj.mensajeError('Error','Error persivido')

    })

  }

  subirImg(evento){

    if(evento.target.files.length > 0 ){

      let nombre = new Date().getTime().toString() //Rescata el tiempo actual

      let archivo = evento.target.files[0] //Es un arreglo de archivosescata el tiempo actual

      let extension = archivo.name.toString().substring(archivo.name.toString().lastIndexOf('.'))

      let ruta = 'clientes/' + nombre + extension //Ruta dentro del Storage FireBase
      const referencia = this.storage.ref(ruta) //Crea la ruta general
      const tarea = referencia.put(archivo)  //Sube el archivo

      tarea.then((objeto)=>{
        referencia.getDownloadURL().subscribe((url)=>{
          this.urlImg = url //Rescata la uRL de la img
        })

      })


      tarea.percentageChanges().subscribe((porcentaje)=>{
        this.porcentajeSubida = parseInt(porcentaje.toString())
        
      })


    }


  }

}
