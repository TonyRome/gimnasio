import { DocumentReference } from '@angular/fire/firestore';

export class Inscripcion{
   
    fecha: Date;
    fechaFinal: Date;
    cliente: DocumentReference;
    precios: DocumentReference;
    subTotal: number
    isv: number
    total: number

    constructor(){

        this.fecha = null
        this.fechaFinal = null
        this.cliente = this.cliente
        this.precios = this.precios
        this.subTotal = this.subTotal
        this.isv = this.isv
        this.total = this.total
        
    }

    validar(): any{
        let respuesta = {
            esValido: false,
            mensaje: ''
        }

        if(this.cliente == null || this.cliente == undefined){

            respuesta.esValido = false
            respuesta.mensaje = 'Seleccione cliente'
            return respuesta
        }

        if(this.fecha == null || this.fecha == undefined){

            respuesta.esValido = false
            respuesta.mensaje = 'No tiene fecha de inicio'
            return respuesta
        }

        if(this.fechaFinal == null || this.fechaFinal == undefined){

            respuesta.esValido = false
            respuesta.mensaje = 'No tiene fecha de inicio'
            return respuesta
        }

        if(this.precios == null || this.precios == undefined){

            respuesta.esValido = false
            respuesta.mensaje = 'No tiene precio'
            return respuesta
        }

        if(this.subTotal == null || this.subTotal == undefined){

            respuesta.esValido = false
            respuesta.mensaje = 'No se puede calcular subtotal'
            return respuesta
        }

        if(this.isv == null || this.isv == undefined){

            respuesta.esValido = false
            respuesta.mensaje = 'No se puede calcular isv'
            return respuesta
        }

        if(this.total == null || this.total == undefined){

            respuesta.esValido = false
            respuesta.mensaje = 'No se puede calcular total'
            return respuesta
        }

        respuesta.esValido = true;
        return respuesta
        
    }

}