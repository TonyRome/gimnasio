import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formularioLogin: FormGroup
  datosCorrectos: boolean = true
  textoError: String

  constructor(private creadorFormulario: FormBuilder, public afAuth: AngularFireAuth, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.formularioLogin = this.creadorFormulario.group({
      email: ['', Validators.compose([
        Validators.required, Validators.email
      ])],
      password:['', Validators.required]
    })
  }

  ingresar(){

    if(this.formularioLogin.valid){
    
    this.spinner.show()

    this.afAuth.auth.signInWithEmailAndPassword(this.formularioLogin.value.email, this.formularioLogin.value.password)
    .then((usuario)=>{ //Como es una promesa, mientras obtiene el usuario como objeto muestra el spinner
      console.log(usuario)
      this.spinner.hide()
    }).catch((error)=>{
      this.datosCorrectos = false
      this.textoError = error.message
      this.spinner.hide() //Si hay error se debe ocultar el spinner
    })

    }
    else{
      this.textoError = "Por favor revisar que datos son correctos"
    }

    
  }

}
