import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'; //EventEmitter tiene que ser de core sino da problemas con variable de entrada
import { AngularFirestore } from '@angular/fire/firestore';
import { Cliente } from '../models/cliente';


@Component({
  selector: 'app-seleccionar-cliente',
  templateUrl: './seleccionar-cliente.component.html',
  styleUrls: ['./seleccionar-cliente.component.scss']
})
export class SeleccionarClienteComponent implements OnInit {

  clientes: Cliente[] = new Array<Cliente>()
  @Input('nombre')  nombre: string //Input permite ingresar valor a variable desde otro componente HTML en la etiqueta EJ: <app-componente nombreInput="valorInput"</>
  @Output('seleccionoCliente') seleccionoCliente = new EventEmitter()
  @Output('canceloCliente') canceloCliente = new EventEmitter()

  constructor(private db: AngularFirestore) { }

  ngOnInit() {
    this.db.collection<any>('clientes').get().subscribe((resultado)=>{
      console.log("Este es el resultado: ",resultado)

      this.clientes.length = 0
      resultado.docs.forEach((item)=>{
        console.log("Este es el resultado.docs: ",resultado.docs)

        console.log("Este es el item: ",item)
        console.log("Este es el item.data()",item.data())

        //ID solo se encuentra en el item no  su cadata
        let cliente: any = item.data() //No viene incluido en la data el ID del FIREBASE
        cliente.id = item.id
        cliente.ref = item.ref
        cliente.visible = false
        this.clientes.push(cliente)

      })
    })
  }

  buscarCliente(nombre: string){
    this.clientes.forEach((cliente)=>{
      if(cliente.nombre.toLowerCase().includes(nombre.toLowerCase())){
        cliente.visible = true
      }
      else{
        cliente.visible = false
      }
    })

  }

  seleccionarCliente(cliente: Cliente){
    this.nombre = cliente.nombre + ' ' + cliente.apellido //Variable Input se ingresa por medio de esat funcion click a
    this.clientes.forEach((cliente)=>{                 //Input del HTML a través de [(ngModel)] para ingresar a su valor como tal  
      cliente.visible = false                            //Y que no muestra el string
    })
    this.seleccionoCliente.emit(cliente) //Se enlaza a component HTML, este esta en inscripcion HTML  <app-seleccionar-cliente (seleccionoCliente)="asignarCliente($event)">                                     
  }

  cancelarCliente(){
    this.nombre = undefined
    this.canceloCliente.emit()
  }

}
