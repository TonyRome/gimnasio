import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { MensajesService } from '../service/mensajes.service';
import {Precio} from '../models/precio';

@Component({
  selector: 'app-precios',
  templateUrl: './precios.component.html',
  styleUrls: ['./precios.component.scss']
})
export class PreciosComponent implements OnInit {
  formularioPrecios: FormGroup
  precios: Precio[] = new Array<Precio>()
  esEditar: boolean = false
  id: string

  constructor(private fb: FormBuilder, private db: AngularFirestore, private msj: MensajesService) { }

  ngOnInit() {

    this.formularioPrecios = this.fb.group({
      nombre:['', Validators.required],
      costo:['', Validators.required], //Aunque sea numero el validador no funcionara si no se pone como string
      duracion:['', Validators.required],
      tipoDuracion:['', Validators.required]
    })

    this.mostraŕPrecios()

  }

  mostraŕPrecios(){
    this.db.collection<Precio>('precios').get().subscribe((resultado)=>{
      this.precios.length = 0 
      resultado.docs.forEach((dato)=>{
        let precio = dato.data() as Precio
      precio.id = dato.id
      precio.ref = dato.ref
      this.precios.push(precio)
      })
    })
  }

  agregar(){
    this.db.collection<Precio>('precios').add(this.formularioPrecios.value).then(()=>{
      this.msj.mensajeCorrecto('Agregado','Exitosa jugada')
      this.formularioPrecios.reset()
      this.mostraŕPrecios()
    }).catch(()=>{
      this.msj.mensajeError('Error','No se pudo completar')
    })
  }

  editarPrecio(precio: Precio){
    
    this.esEditar = true
    this.formularioPrecios.setValue({
      nombre: precio.nombre,
      costo: precio.costo,
      duracion: precio.duracion,
      tipoDuracion: precio.tipoDuracion
    })
    this.id = precio.id
  }

  editar(){
    this.db.doc('precios/'+this.id).update(this.formularioPrecios.value).then(()=>{
      this.msj.mensajeCorrecto('Editado','Exitosa jugada')
      this.formularioPrecios.reset()
      this.esEditar = false
      this.mostraŕPrecios()
    }).catch(()=>{
      this.msj.mensajeError('Error','No se puede ejecutar')
    })
  }

}
