import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Inscripcion } from '../models/inscripcion';

@Component({
  selector: 'app-listado-inscripcion',
  templateUrl: './listado-inscripcion.component.html',
  styleUrls: ['./listado-inscripcion.component.scss']
})
export class ListadoInscripcionComponent implements OnInit {
  inscripciones: any[] = new Array<any>()

  constructor(private db: AngularFirestore) { }

  ngOnInit() {
    this.inscripciones.length = 0 

    this.db.collection('inscripciones').get().subscribe((resultado)=>{

      resultado.forEach((inscripcion)=>{

        let inscripcionObtenida = inscripcion.data()
        inscripcionObtenida.id = inscripcion.id //este atributo id se agrega en este momento yaque el id no viene en atributo cliente

        //Hasta este punto la inscripcionObtenida tiene un atributo cliente pero de tipo DocumentReferent, este cliente sirve solamente para guiarse hacia el cliente original

        this.db.doc(inscripcion.data().cliente.path).get().subscribe((cliente)=>{
          console.log("Ruta cliente: ", cliente.data())
          inscripcionObtenida.clienteObteniedo = cliente.data() //Agrega el cliente normal como document referent y otro que es son los atributos del cliente
          //Igual como el atributo id agregado en el momento, se agrega un atributo nuevo de ClienteObtenido que almacena el cliente dentro de la coleccion cliente junto con sus atributos
          //Por lo que se accede dentro de inscripcion a la path que esta dentro de la variable cliente (Tipo documentreferent) para obtener cliente con valores originales
          
          inscripcionObtenida.fecha = new Date(inscripcionObtenida.fecha.seconds * 1000)
          inscripcionObtenida.fechaFinal = new Date(inscripcionObtenida.fechaFinal.seconds * 1000 )
          
          
          this.inscripciones.push(inscripcionObtenida)
          console.log("Este es el inscripcion con los clientes: ", inscripcionObtenida)
          
        })

      })


    })
  }

}
