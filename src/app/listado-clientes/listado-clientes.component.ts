import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-listado-clientes',
  templateUrl: './listado-clientes.component.html',
  styleUrls: ['./listado-clientes.component.scss']
})
export class ListadoClientesComponent implements OnInit {

  clientes: any[] = new Array<any>()

  constructor(private db: AngularFirestore) { }

  ngOnInit() { 

    //Se tiene que configurar los permisos en FireBase para antes para poder hacer un request a la DDBb
    //Rescata todos los atributos de cada cliente pero no el ID que se le asigna automatica o manual en el FireBase
    // this.db.collection('clientes').valueChanges().subscribe((resultado)=>{
    //   this.clientes = resultado
    // }) //Es mejor colocarlo con subscribe ayaque por el contrario se debe agregar un pipe async y va a dificultar mas agregar un icon loading
  
  //Para obtener el ID de cada cliente se hace lo siguiente:
  this.clientes.length = 0 //Se asegura que el arreglo este vacio
  this.db.collection('clientes').get().subscribe((resultado)=>{ // Se obtiene por medio de get() toda la informacion de clientes
    //Obtiene la columna documentos (docs) y dentro de este obtiene el ID de cada cliente

    resultado.docs.forEach((item)=>{
      // console.log(item.id)
      // console.log(item.data())


      let cliente = item.data()
      cliente.id = item.id
      cliente.ref = item.ref

      this.clientes.push(cliente)
    })
    
  })

  }

}
